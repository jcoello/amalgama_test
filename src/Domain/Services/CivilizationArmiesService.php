<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:06
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\Army;
use Amalgama\Domain\Entities\Civilization;

class CivilizationArmiesService {

	public function initialArmiesToCivilization(Civilization $civilization) {
		$army = new Army();
		$army->addPikemans($civilization->getInitialPikemans());
		$army->addArchers($civilization->getInitialArchers());
		$army->addKnights($civilization->getInitialKnights());
		$army->setCoins(Army::INITIAL_COINS);
		$civilization->addArmy($army);
	}
}
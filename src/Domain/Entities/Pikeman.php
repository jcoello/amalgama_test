<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:08
 */

namespace Amalgama\Domain\Entities;


class Pikeman extends ArmyUnit {

	const INITIAL_FORCE_POINTS = 5;

	public function __construct() {
		$this->setForcePoints(self::INITIAL_FORCE_POINTS);
	}

}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:12
 */

namespace Amalgama\Domain\Entities;

class ChineseCivilization extends Civilization {

	public function __construct() {
		$this->setInitialPikemans(5);
		$this->setInitialArchers(25);
		$this->setInitialKnights(2);
	}
}
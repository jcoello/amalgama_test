<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:14
 */

namespace Amalgama\Domain\factories;


use Amalgama\Domain\Entities\BizantineCivilization;
use Amalgama\Domain\Entities\BritishCiviliation;
use Amalgama\Domain\Entities\ChineseCivilization;
use Amalgama\Domain\Entities\Civilization;
use Amalgama\Domain\Services\CivilizationArmiesService;

class CivilizationFactory {

	const CHINESE_CIVILIZATION = "chinese";
	const BRITISH_CIVILIZATION = "british";
	const BIZANTINE_CIVILIZATION = "bizantine";

	private $civilizationArmiesService;

	public function __construct(
		CivilizationArmiesService $civilizationArmiesService
	) {
		$this->civilizationArmiesService = $civilizationArmiesService;
	}

	public function create($civilizationName) : Civilization {
		$civilization = null;

		switch ($civilizationName) {
			case (self::CHINESE_CIVILIZATION):
				$civilization = new ChineseCivilization();
			case (self::BRITISH_CIVILIZATION):
				$civilization = new BritishCiviliation();
			case (self::BIZANTINE_CIVILIZATION):
				$civilization = new BizantineCivilization();
			default:
				throw new Exception("Civilization not found for name");
		}

		$this->civilizationArmiesService->initialArmiesToCivilization($civilization);

		return $civilization;
	}
}
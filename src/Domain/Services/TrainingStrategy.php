<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 17:28
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\Archer;
use Amalgama\Domain\Entities\ArmyUnit;
use Amalgama\Domain\Entities\Knight;
use Amalgama\Domain\Entities\Pikeman;

class TrainingStrategy {

	private $provider;

	public function __construct(ArmyUnit $armyUnit) {
		switch (get_class($armyUnit)) {
			case (Knight::class):
				$this->provider = new KnightTraining();
			case (Archer::class):
				$this->provider = new ArcherTraining();
			case (Pikeman::class):
				$this->provider = new PikemanTraining();
			default:
				throw new Exception("Training not found for unit");
		}
	}

	public function train(ArmyUnit $armyUnit, $coins) {
		return $this->provider->trainUnit($armyUnit, $coins);
	}
}
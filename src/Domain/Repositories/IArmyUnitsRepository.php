<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:59
 */

namespace Amalgama\Domain\Repositories;


interface IArmyUnitsRepository {
	public function getMoreValuableUnits(Army $army);
}
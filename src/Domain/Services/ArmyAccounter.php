<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:45
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\Army;

class ArmyAccounter {

	public function getTotalPoints(Army $army) {
		$units = $army->getAllUnits();

		$totalPoints = 0;

		foreach ($units as $unit) {
			$totalPoints += $unit->getForcePoints();
		}

		return $totalPoints;
	}

	public function getMostPointedUnits(Army $army) {

	}
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:23
 */

namespace Amalgama\Domain\factories;


use Amalgama\Domain\Entities\Archer;
use Amalgama\Domain\Entities\Knight;
use Amalgama\Domain\Entities\Pikeman;

class ArmyUnitsFactory {
	const PIKEMAN_TYPE = "pikeman";
	const ARCHER_TYPE = "archer";
	const KNIGHT_TYPE = "pikeman";

	public function create($armyUnitType) {
		switch ($armyUnitType) {
			case (self::PIKEMAN_TYPE):
				return new Pikeman();
			case (self::ARCHER_TYPE):
				return new Archer();
			case (self::KNIGHT_TYPE):
				return new Knight();
			default:
				throw new \Exception("Army unit with type not found.");
		}
	}
}
<?php

use Amalgama\Domain\Entities\Archer;
use Amalgama\Domain\Entities\Pikeman;
use Amalgama\Domain\Services\PikemanTraining;
use Mockery\Adapter\Phpunit\MockeryTestCase;

/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 17:37
 */

class PikemanTrainingTest extends MockeryTestCase {

	private $pikemanTraining;

	public function setup() {
		$this->pikemanTraining = new PikemanTraining();
	}

	public function testReturnArcherIfCoinsForArcherArePayed() {
		$pikeman = new Pikeman();
		$pikeman->setForcePoints(20);
		$coins = 100;

		$response = $this->pikemanTraining->trainUnit($pikeman, $coins);
		$this->assertInstanceOf(Archer::class, $response);
	}

	public function testAdd9PointsIf30CoinsSended() {
		$pikeman = new Pikeman();
		$pikeman->setForcePoints(10);
		$coins = 30;

		$response = $this->pikemanTraining->calculateNewForce($pikeman, $coins);

		$this->assertEquals(19, $response);
	}
}
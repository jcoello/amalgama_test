<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:19
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\Archer;
use Amalgama\Domain\Entities\ArmyUnit;
use Amalgama\Domain\Entities\Pikeman;

class PikemanTraining implements Training {

	private $transformCost = 30;
	private $upgradePoints = 3;
	private $upgradeForceCost = 10;

	public function trainUnit(ArmyUnit $armyUnit, $coins): ArmyUnit {

		if($coins > $this->transformCost) {
			return new Archer();
		}

		$unitNewForce = $this->calculateNewForce($armyUnit, $coins);

		$armyUnit->setForcePoints($unitNewForce);

		return $armyUnit;
	}

	/**
	 * @param ArmyUnit $armyUnit
	 * @param $coins
	 * @return float|int|mixed
	 */
	public function calculateNewForce(ArmyUnit $armyUnit, int $coins) {
		$unitCurrentForce = $armyUnit->getForcePoints();
		$upgradesQuantity = $coins / $this->upgradeForceCost;
		$toAddForce = $this->upgradePoints * $upgradesQuantity;
		$unitNewForce = $unitCurrentForce + $toAddForce;
		return $unitNewForce;
	}
}
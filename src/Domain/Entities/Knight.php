<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:11
 */

namespace Amalgama\Domain\Entities;


class Knight extends ArmyUnit {
	const INITIAL_POINTS = 20;

	public function __construct() {
		$this->setForcePoints(self::INITIAL_POINTS);
	}
}
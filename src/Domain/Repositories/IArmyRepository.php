<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 17:07
 */

namespace Amalgama\Domain\Repositories;


use Amalgama\Domain\Entities\Army;

interface IArmyRepository {
	public function removeUnits(Army $army, $units);
	public function addCoins(Army $army, int $coins);
}
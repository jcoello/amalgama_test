<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 17:12
 */

namespace Amalgama\Domain\Repositories;


use Amalgama\Domain\Entities\Army;

interface IArmyBattleHistoryRepository {
	public function addBattle(Army $army, $battle);
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:06
 */

namespace Amalgama\Domain\Entities;


abstract class ArmyUnit {
	private $forcePoints;

	/**
	 * @return mixed
	 */
	public function getForcePoints() {
		return $this->forcePoints;
	}

	/**
	 * @param mixed $forcePoints
	 */
	public function setForcePoints($forcePoints): void {
		$this->forcePoints = $forcePoints;
	}
}
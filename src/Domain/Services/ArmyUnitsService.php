<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:55
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\Army;

class ArmyUnitsService {

	public function __construct(
		ArmyUnitsFactory $armyUnitsFactory
	) {
		$this->armyUnitsFactory = $armyUnitsFactory;
	}

	public function addPickers(Army $army, int $pickersQuantity) {
		$pikemans = array();

		for($i = 0; $i < $pickersQuantity; $i++) {
			$pikemans[] = $this->armyUnitsFactory(ArmyUnitsFactory::PIKEMAN_TYPE);
		}

		$army->addPikemans($pikemans);
	}

	public function addArchers(Army $army, int $archersQuantity) {
		$archers = array();

		for($i = 0; $i < $archersQuantity; $i++) {
			$this->archers[] = $this->armyUnitsFactory(ArmyUnitsFactory::ARCHER_TYPE);
		}

		$army->addArchers($archers);
	}

	public function addKnights(Army $army, int $knightsQuantity) {
		$knights = array();

		for($i = 0; $i < $knightsQuantity; $i++) {
			$this->archers[] = $this->armyUnitsFactory(ArmyUnitsFactory::KNIGHT_TYPE);
		}

		$army->addKnights($knights);
	}
}
<?php

namespace Amalgama\Domain\Entities;

/**
 */
class Army {

	const INITIAL_COINS = 1000;

	protected $pikemans;
	protected $archers;
	protected $knights;
	protected $civilization;
	protected $coins;
	protected $battleHistory;

	/**
	 * @return mixed
	 */
	public function getBattleHistory() {
		return $this->battleHistory;
	}

	/**
	 * @param mixed $battleHistory
	 */
	public function setBattleHistory($battleHistory): void {
		$this->battleHistory = $battleHistory;
	}

	/**
	 * @return mixed
	 */
	public function getCoins() {
		return $this->coins;
	}

	/**
	 * @param mixed $coins
	 */
	public function setCoins($coins): void {
		$this->coins = $coins;
	}

	/**
	 * @return mixed
	 */
	public function getCivilization() {
		return $this->civilization;
	}

	/**
	 * @param mixed $civilization
	 */
	public function setCivilization($civilization): void {
		$this->civilization = $civilization;
	}

	/**
	 * @return mixed
	 */
	public function getPikemans() {
		return $this->pikemans;
	}

	/**
	 * @return mixed
	 */
	public function getArchers() {
		return $this->archers;
	}

	public function getAllUnits() {
		$units = array();

		$units[] = $this->getPikemans();
		$units[] = $this->getArchers();
		$units[] = $this->getKnights();

		return $units;
	}

	/**
	 * @return mixed
	 */
	public function getKnights() {
		return $this->knights;
	}

	public function addPikemans($pikemans) {
		$this->pikemans[] = $pikemans;
	}

	public function addArchers($archers) {
		$this->archers[] = $archers;
	}

	public function addKnights($knights) {
		$this->knights = $knights;
	}

	public function removeUnit(ArmyUnit $armyUnit) {
		// TODO remove unit
	}
}
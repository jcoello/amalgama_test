<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:10
 */

namespace Amalgama\Domain\Entities;


class Archer extends ArmyUnit {

	const INITIAL_POINTS = 10;

	public function __construct() {
		$this->setForcePoints(self::INITIAL_POINTS);
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:07
 */

namespace Amalgama\Domain\Entities;


class Battle {

	private $attackingArmy;
	private $defendingArmy;
	private $losingArmy;

	/**
	 * @return mixed
	 */
	public function getAttackingArmy() {
		return $this->attackingArmy;
	}

	/**
	 * @param mixed $attackingArmy
	 */
	public function setAttackingArmy($attackingArmy): void {
		$this->attackingArmy = $attackingArmy;
	}

	/**
	 * @return mixed
	 */
	public function getDefendingArmy() {
		return $this->defendingArmy;
	}

	/**
	 * @param mixed $defendingArmy
	 */
	public function setDefendingArmy($defendingArmy): void {
		$this->defendingArmy = $defendingArmy;
	}

	/**
	 * @return mixed
	 */
	public function getLosingArmy() {
		return $this->losingArmy;
	}

	/**
	 * @param mixed $losingArmy
	 */
	public function setLosingArmy($losingArmy): void {
		$this->losingArmy = $losingArmy;
	}

	/**
	 * @return mixed
	 */
	public function getWiningArmy() {
		return $this->winingArmy;
	}

	/**
	 * @param mixed $winingArmy
	 */
	public function setWiningArmy($winingArmy): void {
		$this->winingArmy = $winingArmy;
	}
	private $winingArmy;
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:10
 */

namespace Amalgama\Domain\Entities;


class ArmyBattleHistory {
	private $army;
	private $battles;

	/**
	 * @return mixed
	 */
	public function getBattles(): array {
		return $this->battles;
	}

	/**
	 * @param mixed $battle
	 */
	public function addBattle(Battle $battle): void {
		$this->battles = $battle;
	}

	/**
	 * @return mixed
	 */
	public function getArmy(): Army {
		return $this->army;
	}

	/**
	 * @param mixed $army
	 */
	public function setArmy(Army $army): void {
		$this->army = $army;
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:13
 */

namespace Amalgama\Domain\Entities;


class BizantineCivilization extends Civilization {

	public function __construct() {
		$this->setInitialPikemans(5);
		$this->setInitialArchers(8);
		$this->setInitialKnights(15);
	}
}
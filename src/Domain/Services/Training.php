<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:18
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\ArmyUnit;

interface Training {
	public function trainUnit(ArmyUnit $armyUnit, $coins): ArmyUnit;
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:52
 */

namespace Amalgama\Domain\factories;


use Amalgama\Domain\Entities\Army;

class ArmyFactory {
	public function create(): Army {
		return new Army();
	}
}
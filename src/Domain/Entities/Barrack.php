<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:13
 */

namespace Amalgama\Domain\Entities;

use Amalgama\Domain\Services\TrainingStrategy;

class Barrack {

	public $trainingProvider;

	public function train(ArmyUnit $armyUnit, int $coins) {
		$training = new TrainingStrategy($armyUnit);
		$trainedUnit = $training->trainUnit($armyUnit, $coins);

		return $trainedUnit;
	}
}
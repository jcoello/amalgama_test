<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 16:43
 */

namespace Amalgama\Domain\Services;


use Amalgama\Domain\Entities\Army;
use Amalgama\Domain\Entities\ArmyBattleHistory;
use Amalgama\Domain\Entities\Battle;
use Amalgama\Domain\Repositories\IArmyRepository;
use Amalgama\Domain\Repositories\IArmyUnitsRepository;

class BattleService {

	const WINNED_BATTLE_COINS = 100;

	private $armyUnitsRepository;
	private $armyRepository;

	public function __construct(
		IArmyUnitsRepository $armyUnitsRepository,
		IArmyRepository $armyRepository
	) {
		$this->armyUnitsRepository = $armyUnitsRepository;
		$this->armyRepository = $armyRepository;
	}

	public function runBattle(Army $attackingArmy, Army $defendingArmy) {
		$attackingArmyPoints = $attackingArmy->getTotalPoints();
		$defendingArmyPoints = $defendingArmy->getTotalPoints();

		$battle = new Battle();

		if($attackingArmyPoints > $defendingArmyPoints) {
			$winingArmy = $attackingArmy;
			$losingArmy = $defendingArmy;
		} else {
			$winingArmy = $defendingArmy;
			$losingArmy = $attackingArmy;
		}

		$battle->setWiningArmy($winingArmy);
		$battle->setLosingArmy($losingArmy);

		$attackingArmyBattleHistory = new ArmyBattleHistory();
		$attackingArmyBattleHistory->setArmy($attackingArmy);
		$attackingArmyBattleHistory->addBattle($battle);

		$defendingArmyBattleHistory = new ArmyBattleHistory();
		$defendingArmyBattleHistory->setArmy($attackingArmy);
		$defendingArmyBattleHistory->addBattle($battle);

		$toRemoveUnits = $this->armyUnitsRepository->getMoreValuableUnits($losingArmy);
		$this->armyRepository->removeUnits($losingArmy, $toRemoveUnits);
		$this->armyRepository->addCoins($winingArmy, self::WINNED_BATTLE_COINS);
	}

}
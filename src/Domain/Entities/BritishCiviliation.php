<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:12
 */

namespace Amalgama\Domain\Entities;


class BritishCiviliation extends Civilization {
	public function __construct() {
		$this->setInitialPikemans(10);
		$this->setInitialArchers(10);
		$this->setInitialKnights(10);
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: fizzmod-54
 * Date: 29/09/18
 * Time: 15:06
 */

namespace Amalgama\Domain\Entities;

class Civilization {

	protected $initialPikemans;
	protected $initialArchers;
	protected $initialKnights;

	protected $armies;

	/**
	 * @return mixed
	 */
	public function getArmies() {
		return $this->armies;
	}

	/**
	 * @param mixed $army
	 */
	public function addArmy($army): void {
		$this->armies[] = $army;
	}

	/**
	 * @return mixed
	 */
	public function getInitialPikemans() {
		return $this->initialPikemans;
	}

	/**
	 * @param mixed $initialPikemans
	 */
	public function setInitialPikemans($initialPikemans): void {
		$this->initialPikemans = $initialPikemans;
	}

	/**
	 * @return mixed
	 */
	public function getInitialArchers() {
		return $this->initialArchers;
	}

	/**
	 * @param mixed $initialArchers
	 */
	public function setInitialArchers($initialArchers): void {
		$this->initialArchers = $initialArchers;
	}

	/**
	 * @return mixed
	 */
	public function getInitialKnights() {
		return $this->initialKnights;
	}

	/**
	 * @param mixed $initialKnights
	 */
	public function setInitialKnights($initialKnights): void {
		$this->initialKnights = $initialKnights;
	}
}